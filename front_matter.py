import os, yaml, datetime, time
import frontMatterPrompt as fmp  
# 使用 Qt Designer 设计的 ui 转成的 py 脚本
from PyQt5.QtWidgets import *
import sys

class FrontMatter():
    '''
    对一个markdown文件的front matter 进行读取和处理
    '''
    def __init__(self, fp, gui=False):
        '''
        初始化就读取yaml
        '''
        self.filepath = fp
        base, ext = os.path.splitext(fp)
        self.wks, self.filename = os.path.split(base)
        self.info = {}  # 先定义为一个空字典
        self.cut_line = '---\n'
        self.main_text = ''
        if ext!='.md':
            raise Exception(f'{ext}为不支持的文件格式!')   # raise 之后程序直接中断了
        if os.path.exists(fp):
            self.load()
        self.info_ = self.info.copy()
        if gui:
            self.ui = fmp.Ui_MainWindow()
            self.MainWindow = QMainWindow()
            self.ui.setupUi(self.MainWindow)
            self.MainWindow.setWindowTitle(f'{self.filename}')
            self.connectAction()
            self.reset()

    def connectAction(self):
        self.ui.update.clicked.connect(self.update)
        self.ui.reset.clicked.connect(self.reset)
        self.ui.other.clicked.connect(self.other)

    def getInfos(self):
        self.title = self.ui.title.text()
        self.categories = self.ui.categories.text()
        self.tags = self.ui.tags.text()
        self.excerpt = self.ui.excerpt.text()

    def update(self):
        self.getInfos()
        self.info['title'] = self.title
        self.info['categories'] = self.categories
        self.info['tags'] = self.tags
        self.info['excerpt'] = self.excerpt
        self.save()
        self.ui.tips.setText('已更新信息!')
    
    def reset(self):
        if 'title' in self.info_.keys():
            self.ui.title.setText(self.info_['title'])
        if 'categories' in self.info_.keys():
            self.ui.categories.setText(self.info_['categories'])
        if 'tags' in self.info_.keys():
            self.ui.tags.setText(self.info_['tags'])        
        if 'excerpt' in self.info_.keys():
            self.ui.excerpt.setText(self.info_['excerpt'])
        self.info = self.info_.copy()

    def other(self):
        dd = 'top: true'
        message, ok = QInputDialog.getText(self.MainWindow, 
                "新增项", "键值对:", QLineEdit.Normal, "{}".format(dd))
        key, value = message.split(':')
        value = value.strip()
        self.setItem(key, value)
        self.ui.tips.setText('已新增键值对！')

    def load(self):
        '''
        文件存在的时候就读取，并提取yaml信息
        文件不存在，比如要创建新文稿的时候，就无需 load
        '''
        with open(self.filepath, 'r', encoding='utf-8') as f:
            self.content = f.read()
        if not self.content.startswith(self.cut_line):
            self.main_text = self.content
            title = self.main_text.split('\n')[0]  # 截取第一行作为标题
            title = title.strip().strip('#').strip()
            timeStamp = os.path.getctime(self.filepath)  # 取文件创建时间为date，返回浮点值
            self.info['title'] = title
            self.info['date'] = datetime.datetime.fromtimestamp(int(timeStamp))
            # raise Exception('此文件内不存在 front matter!') 
            print('此文件内不存在 front matter!\n{}'.format(self.filepath))
        else:
            self.raw_info = self.content.split(self.cut_line)[1]
            self.main_text = self.content.split(self.cut_line)[2]
            self.info = yaml.load(self.raw_info, Loader=yaml.FullLoader)
    
    def _test_yaml_info(self):
        print(self.info)

    def _test_main_text(self):
        print(self.main_text)
    
    def _test_date(self):
        print(self.info['date'])
        print(type(self.info['date']))
    
    def _test_datetime(self):
        a = time.time()
        b = datetime.datetime.fromtimestamp(int(a))
        print(a)
        print(b)

    def setItem(self, key, value):
        '''
        向 front matter 中添加内容，会直接覆盖原有内容，相当于提供了一个修改接口（Input）
        '''
        self.info[key] = value
    
    def save(self):
        '''
        更新并保存信息到文稿的front_matter中（Output）
        '''
        info = yaml.dump(self.info, default_flow_style=False, encoding='utf-8', allow_unicode=True, sort_keys=False)
        # https://blog.csdn.net/weixin_41548578/article/details/90651464
        # https://www.cnblogs.com/pandaly/p/13274150.html
        # yaml dump 的时候支持中文，显示 emoji，禁止排序
        # print(a.decode('utf-8'))
        self.content = self.cut_line + info.decode('utf-8') +self.cut_line + self.main_text
        with open(self.filepath, 'w', encoding='utf-8') as f:
            f.write(self.content)


if __name__=='__main__':
    app = QApplication(sys.argv)
    fp = '2021-09-24_测试一下.md'
    xxd = FrontMatter(fp, gui=True)
    xxd.MainWindow.show()
    sys.exit(app.exec_())