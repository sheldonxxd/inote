import os, glob, subprocess

class Convertor():
    '''
    借助pandoc命令行工具将markdown文件转成嵌入式html或者带引文的docx
    '''
    def __init__(self, fp, sty):
        '''
        fp: markdown 文稿的绝对路径
        sty: pandoc 转格式所需的额外文件所在目录
        导出文件放到文稿附录中
        '''
        self.fp = fp
        base, ext = os.path.splitext(fp)
        self.asset = base+'/'
        self.basename = os.path.split(base)[-1]
        self.wks, self.fname = os.path.split(fp)
        self.files = {}
        exts = ['csl', 'docx', 'css', 'bib']
        for ext in exts:
            a = glob.glob(os.path.join(sty, f'*.{ext}'))[0]
            if not os.path.exists(a):
                raise Exception(f'{sty} 目录下 {ext} 文件缺失！')
            self.files[ext] = a
    
    def toHTML(self):
        target = os.path.join(self.asset, self.basename+'.html')
        css = self.files['css']
        command = f'pandoc -t html5 -s {self.fp} --from markdown+emoji --webtex --self-contained -c {css} -o {target}'
        self.run(command)

    def toDOCX(self):
        target = os.path.join(self.asset, self.basename+'.docx')
        bib = self.files['bib']
        csl = self.files['csl']
        template = self.files['docx']
        command = f'pandoc --citeproc --from markdown+emoji --bibliography={bib} --csl={csl} --reference-doc={template} {self.fp} -o {target}'
        self.run(command)
    
    def run(self, command):
        try:
            ret = subprocess.Popen(command, shell=True, cwd=self.wks)
        except Exception as e:
            raise Exception(str(e))

if __name__=='__main__':
    fp = '/run/media/xxd/T7/codes/others/gitlab-repos/实验记录本/readme.md'
    sty = '/run/media/xxd/T7/plugin/pandoc'
    xxd = Convertor(fp, sty)
    xxd.toDOCX()
    xxd.toHTML()