import time, os

class Template():
    '''
    根据模板文件自动生成内容
    借鉴 hexo、obsidian 等方案
    将{{command}}进行转换
    '''
    default_
    def __init__(self, fp):
        '''
        fp: 模板文件所在位置
        '''
        self.command_list = ['date', 'time', 'datetime']
        # 还可以添加个天气，地址什么的，但是没有太大的必要
        self.filepath = fp
        if os.path.exists(fp):
            self.content = self.load()
        else:
            # 如果不存在，则使用默认模板
            self.content = self.default_template
    
    def datetime_convert(self, key):
        fmts = {
            'date': '%Y-%m-%d',
            'time': '%H:%M:%S',
            'datetime': '%Y-%m-%d %H:%M:%S',
        }
        return self.getDateTimeString(fmts[key])
    
    def getDateTimeString(self, fmt='%Y%m%d_%H%M%S'):
        tt = time.time()
        tl = time.localtime(tt)
        dd = time.strftime(fmt,tl)
        return dd

    def load()
    def _test_convert(self):
        print(self.convert('date'))
        print(self.convert('time'))


if __name__=='__main__':
    xxd = Template()
    xxd._test_convert()