import os, glob, re

class Search:
    def __init__(self, wks, content):
        '''
        在 wks 目录下的所有 markdown 文件中查找内容
        目前只支持 AND 搜索和未经测试的正则表达式搜索，
        关键词用空格件隔开，搜索结果为列表返回
        '''
        self.re_flag = False  # 启用正则搜索
        self.postdir = wks 
        if content.startswith('*') and len(content)>3:
            self.re_flag = True
            self.pattern = content[1:]
        else:
            self.keywords = content.split(' ')
        self.files = sorted(glob.glob(os.path.join(wks, '*.md')), key=os.path.getmtime, reverse=True)
        self.results = [] 
    
    def open(self):
        '''
        读取所有文件并汇总到 self.database 中
        '''
        self.database = []
        for f in self.files:
            with open(f, 'r', encoding='utf-8') as se:
                self.database.append([f, se.read()])
    
    def match(self):
        if not self.re_flag:
            pattern = '|'.join(self.keywords)
            n = len(self.keywords)
            try:
                for idx, rec in enumerate(self.database):
                    fpath, content = rec 
                    results = re.findall(pattern, content)
                    rn = len(list(set(results)))
                    if rn == n:
                        fbase, _ = os.path.splitext(fpath)
                        _, fname = os.path.split(fbase)
                        self.results.append(fname)
            except:
                self.results.append('Error: keywords invalid!')
        else:
            pattern = self.pattern
            try:
                for idx, rec in enumerate(self.database):
                    fpath, content = rec 
                    result = re.search(pattern, content, re.DOTALL)
                    if result is not None:
                        fbase, _ = os.path.splitext(fpath)
                        _, fname = os.path.split(fbase)
                        self.results.append(fname)
            except:
                self.results.append('Error: keywords invalid!')