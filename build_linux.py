#--------linux 平台下用 pyinstaller 打包到用户根目录 iNote-linux-build
import os, sys, shutil

class Build:
    def __init__(self):
        paths = {}
        rootdir = os.path.dirname(sys.argv[0])
        self.rootdir = rootdir
        paths['ico_filepath'] = rootdir+'/icons/book.ico'
        self.paths = paths

    def run(self):
        os.system('cd {} && pyinstaller -F -w main.py -i {}'.format(self.rootdir, self.paths['ico_filepath']))

    def copy(self):
        exe_path = self.rootdir+'/dist/main'
        target = os.path.expanduser('~/iNote-linux-build')
        if not os.path.exists(target):
            os.mkdir(target)      
        shutil.copy(exe_path, target+'/iNote')
    
    def clear(self):
        paths = {}
        paths['main_spec'] = self.rootdir+'/main.spec'
        paths['build'] = self.rootdir+'/build/'
        paths['exe'] = self.rootdir+'/dist/'
        for key in paths:
            if os.path.isfile(paths[key]):
                os.remove(paths[key])
            elif os.path.isdir(paths[key]):
                shutil.rmtree(paths[key])

if __name__ == '__main__':
    worker = Build()
    worker.run()
    worker.copy()
    worker.clear()