import os, pyperclip, shutil

class Attach():
    '''
    给指定文稿添加各类附件
    '''
    def __init__(self, fp, files):
        '''
        fp: markdown 文稿的绝对地址
        files: 要插入的文件路径
        '''
        self.fp = fp
        base, ext = os.path.splitext(fp)
        self.asset = base+'/'
        if not os.path.exists(self.asset):
            os.mkdir(self.asset)
        self.wks, self.fname = os.path.split(fp)
        self.files = files

    def copyToAsset(self):
        '''
        将文件拷贝到附录
        '''
        for fp in self.files:
            _, fname = os.path.split(fp)
            fp2 = os.path.join(self.asset, fname)
            try:
                shutil.copy(fp, fp2)
            except Exception as e:
                print(str(e))

    def makeLink(self):
        '''
        根据文件类型生成相应的markdown引用文本，并复制到粘贴板
        '''
        base, ext = os.path.splitext(self.fp)
        _, fname = os.path.split(base)
        content = []
        for fp in self.files:
            base, ext = os.path.splitext(fp)
            _, caption = os.path.split(base)
            url = f'{fname}/{caption+ext}'
            if ext in ['.png', '.jpg', '.jpeg', '.gif']:
                line = f'\n![{caption}]({url})\n'
            elif ext in ['.mp3', '.m4a']:
                url = url.replace(' ', '&nbsp;')  # html 中的空格要换掉
                line = f'\n<audio controls src="{url}"></audio>\n'
            elif ext in ['.mp4', '.webm']:
                url = url.replace(' ', '&nbsp;')
                line = f'\n<video controls src="{url}"></video>\n'
            else:
                line = f'\n📄[{caption}]({url})\n'
            content.append(line)
        content = '\n'.join(content)
        pyperclip.copy(content)
    
    def run(self):
        '''
        一键执行操作，完成插入附件
        '''
        self.copyToAsset()
        self.makeLink()


if __name__=='__main__':
    files = [
        r"C:\Users\xxd\Desktop\微信图片_20210829113922.jpg",
        r"C:\Users\xxd\Desktop\微信图片_20210829113934.jpg",
        r"C:\Users\xxd\Desktop\Exported Items.bib",
    ]
    fp = r"X:\projects\2021\2021-09-26_test.md"
    xxd = Attach(fp, files)
    xxd.run()