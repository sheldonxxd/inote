---
title: 😄iNote 帮助文档
date: 2021-07-26 10:24:21
categories: 帮助
tags: iNote
excerpt: ''
---

## iNote 简介

iNote 专为「电子实验记录」而生：

1. 内嵌整合了流行美观的 typora 文本编辑器，可轻松撰写图文并茂的笔记
2. 内嵌整合了 git 工具，轻松实现记录的跟踪和内容版本控制
3. 简单的用户界面，方便您管理搜索内容，快速访问日志记录及其附属资源
4. 丰富的右键菜单，帮助您向实验记录中插入图片、音频、视频以及其它各种文件
5. 实用的导出功能，可导出为 html，带参考文献的 docx 或者 zip 压缩包
6. 便捷的资料备份和恢复功能，采用增量备份方案，节省备份所需空间和时间

## iNote 界面概览

![image-20210726102031168](README/image-20210726102031168.png)

*注：此为用户界面基本设计，新增功能主要是添加 actionButton，未必及时更新*

## iNote 安装与配置

**1️⃣下载文件解压**

百度网盘链接：https://pan.baidu.com/s/1LTuK1ffHfXWII9oN3qJHug

提取码：v273

iNote 目前支持 windows 系统和 manjaro-linux 系统，请使用 [7z 工具](https://sparanoid.com/lab/7z/) 解压，解压后目录如下：


**2️⃣修改注册表**

双击运行`iNote.exe`，第一次打开会自动生成`iNote.reg`注册表文件，双击注册表文件，一路同意或者OK，这样您就可以把 iNote 添加到系统右键菜单中，可以右键单击任意文件夹使用 iNote 打开了。

## iNote 使用方法

**1️⃣运行 iNote**

针对每一个课题，新建一个文件夹📂，然后右键单击，选择使用`iNote` 打开即可。打开一个项目后，强烈推荐 `Settings > Shortcut` 在桌面创建快捷方式，下次直接双击即可使用 iNote 打开指定文件夹。

**2️⃣新建文稿（Ctrl+N）**

点击菜单栏中 File 可以寻找到按钮，需要注意的是：

- 新建文件，文件名默认为时间戳，您也可以根据情况修改
- 新建文件自带 front matter，请注意修改标题（title）
- 如果你希望文件在列表中置顶显示，则标题以 🚀、🔥、⏳三种emoji 表情开头，如`title: 🔥这篇文章可以置顶`。
- front matter 目前支持 `abstract`，`categories`，`tags` 等字段，分类一篇文章建议仅为一个，标签不超过五个，如果有多个分类和标签，使用空格分开。

**3️⃣标题标签举例**

| emoji | 名称   | 备注               |
| ----- | ------ | ------------------ |
| ⭐     | 星星   | 周小结、小组会     |
| 📊     | 图表   | 数据分析           |
| 🔨     | 锤子   | 做实验             |
| 🎨     | 调色板 | 作图               |
| 💎     | 钻石   | 非常重要           |
| 👀     | 眼睛   | 阅读文献或调研     |
| 🎯     | 飞镖   | 计划安排目标       |
| 🚌     | 巴士   | 出差               |
| 💻     | 电脑   | 写代码             |
| 🔍     | 放大   | 搜索文献或网络资源 |
| 🛒     | 购物   | 买试剂耗材仪器设备 |

*标题标签有助于通过图形快速对文稿的类别和性质做分类*

**4️⃣使用git版本控制（Ctrl+G）**

对于任一项目文件夹内，首次使用 git 版本控制功能，需要`File>Commit` 提交版本注释，完成版本控制的初始化。

后续使用，程序启动时会自动提交修改记录，实现版本控制。当然如果你取得了关键的重要的进展，建议手动 commit，并且输入自定义注释信息，如`某某实验成功！`。手动提交的将被记录到`history.md`文件中，并自动加入时间戳，在软件下方消息框随机动态显示。

**5️⃣搜索关键词**

- 搜索框输入为空或关键词长度小于2时，搜索会自动刷新当前文件列表
- 多关键词搜索，使用空格键隔开
- 正则搜索仅支持一条表达式，需要以`*`作为前缀，且`.`能够匹配`\n`（换行符）
- 输入`help`搜索，会自动打开 iNote 的帮助文档网页
- 支持日期范围搜索语法，输入`date:20210714:3`，可以搜索 2021年7月14日之后三天内的文稿，对应的`date:20210714:-3`则表示三天之前的文稿。
- 搜索框输入内容以`:`开头，进入message状态，输入内容将被记录到`message.md`文件中，并自动加入时间戳，在软件下方消息框随机动态显示。

**6️⃣搜索结果右键菜单**

- `cite`：引用该文稿，生成`🔗[《title》](filepath)`到粘贴板
- `related`：显示选中文稿内引用的其它相关文稿
- `openAssets`：打开文稿附录文件夹
- `addImage`：向文稿中插入图片资源（可以是多张），文件会自动拷贝到附录，如果资源已经存在于附录，您可以直接把文件拖拽到文稿中，引用文本自动拷贝到粘贴板，下同。
- `addAudio`：向文稿中插入音频资源
- `addMovie`：向文稿中插入视频资源
- `addOthers`：向文稿中插入其它文件资源
- `toArchive`：将文稿归类到指定分类
- `toDesktop`：将文稿及其附录资源拷贝到桌面 Export 目录并压缩（文件后缀名为`.iNote.zip`）
- `toHTML`：依赖pandoc插件，将文稿导出为可离线访问的独立 HTML 文件（可嵌入音频和视频）到用户桌面
- `toDOCX`：依赖pandoc插件，将文稿导出为带有引用参考文献的docx文档到用户桌面
- `toTrash`：将文稿及其附录资源移动至桌面 Trash 目录
- `copy`：将文稿绝对路径拷贝至粘贴板
- `paste`：与`copy`组合使用，能够把一个文库的文稿及其附录资源复制粘贴到另外一个文库中
- `import`：导入后缀名为`.iNote.zip`的文件到文库中
- `post`：可以修改文稿的 Front-Matter，包括标题、摘要、分类和标签等信息
- `gather`：多选状态下(`Alt+C`可切换单选多选状态)的功能，可基于多个文稿生成新的汇总文件。

**7️⃣ Archive说明**

对于科研课题相关文稿资料，大体上按照 paper 分为以下几类：

- `Favorites`：一般性收藏
- `References`：课题相关参考资料，包括文献和其它资源
- `Methods`：课题相关的有效的重要的实验方法
- `Materials`：所使用到的一些实验试剂耗材设备仪器，包括采购报销记录
- `Results`：一些重要的有效的实验分析结果
- `Discussion`：小组会或者一般性学术交流
- `Summary`：对课题的思考，比如创新性、重要性和展望

特别提示，Archive 属于后置行为，一般在文稿完成后定期进行分类，与标题 emoji 搭配使用，事半功倍。分类后有助于撰写文章时查找相关内容。

**8️⃣ 快捷键**

- `Enter`：搜索或提交文本输入框内容
- `Ctrl`+`N`：新建文稿
- `Ctrl`+`G`：提交修改
- `Ctrl`+`T`：窗口置顶
- `Alt`+`C`：切换列表单/多选模式

## Vscode 使用技巧

vscode 是 iNote 强烈推荐并完美适配的文本编辑器，使用简单，界面简洁优美，功能强大，是您撰写试验记录的不二选择。

安装 vscode 之后，一定要安装 Markdown All in One、Paste Image、Foam 等插件，以支持更便捷丰富的 markdown 编辑预览功能。

[Markdown语法及原理从入门到高级（可能是全网最全） - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/99319314)

[Markdown 教程 | 菜鸟教程 (runoob.com)](https://www.runoob.com/markdown/md-tutorial.html)

## Git 版本控制

版本控制最主要的功能就是追踪文件的变更。它将什么时候、什么人更改了文件的什么内容等信息忠实地了记录下来。iNote 采用 git 对实验记录进行版本控制，提供了简单的 `commit` 按钮实现一键式提交修改到本地仓库。您可以使用 [github desktop](https://desktop.github.com/) 等工具可视化追溯查看文件修改记录。

[Git - 关于版本控制 (git-scm.com)](https://git-scm.com/book/zh/v2/起步-关于版本控制)

[如何学习版本控制工具Git？ - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/158038037)

[版本控制系统 - GIT - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/258072004)

## 配套工具推荐

在撰写日志的过程中，好用的截图和屏幕录制工具必不可少，这里强烈推荐 `snipaste` 和 `screen2gif` 两个工具。用户可自行从上边提供的百度云分享链接下载，解压即可使用。

相关介绍资料如下：

[最强截图软件：snipaste - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/99782135)

[GIF截图工具推荐——screenToGIF。_wsdchong的博客-CSDN博客](https://blog.csdn.net/weixin_42875245/article/details/109046079)

## 插入参考文献的说明

注意此说明仅针对导出具有参考文献的docx文档功能进行。默认状态下，程序自动寻找选定文稿附录中的 `refs.bib` 读取文献条目记录。您可以使用 Mendeley、Zotero、JabRef 等文献管理工具导出文献条目为 refs.bib 文件，然后添加到文稿附录中。在 markdown 文稿中插入引用时使用 `[@CitationKey;@CitationKey2]`的方式。

对于 Mendeley，您可以生成一个跟文献库实时同步的 library.bib（Mendeley>Tools>Option>BibTeX>Enable BibTex syncing），然后修改 iNote解压目录中 plugin/pandoc/MD2DOCX.bat 文件。将 `set bibpath=%~f2` 修改为 `set bibpath="path/to/library.bib"`，注意`path/to/library.bib` 是 Mendeley 生成实时同步的 library.bib 的绝对路径。

## 一些奇奇怪怪的问题

- manjaro-linux系统无法正确显示 emoji，可以参考[修改全局字体配置](https://forum.manjaro.org/t/howto-enable-emoji-fonts/36695)

